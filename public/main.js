function addClickListener(listener, items) {
  items.forEach(function(item) {item.addEventListener('click', listener)})
}

function removeWithoutRepaint(event) {
  event.target.classList.remove('has-transition');
  event.target.style.marginLeft = '100px';
  event.target.classList.add('has-transition');
  event.target.style.marginLeft = '50px';
}

function removeWithRepaint(event) {
  event.target.classList.remove('has-transition');
  event.target.style.marginLeft = '100px';
  event.target.offsetHeight; // force a repaint
  event.target.classList.add('has-transition');
  event.target.style.marginLeft = '50px';
}

window.onload = function() {
  const firstExampleItems = document.querySelectorAll('.example-1 li');
  const secondExampleItems = document.querySelectorAll('.example-2 li');
  addClickListener(removeWithoutRepaint, firstExampleItems);
  addClickListener(removeWithRepaint, secondExampleItems);
};
